/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#define LOG_TAG "lights"

#include <cutils/log.h>

#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <pthread.h>

#include <sys/ioctl.h>
#include <sys/types.h>

#include <hardware/lights.h>
#include <cutils/properties.h>

#define MAX_BRIGHTNESS 255

/******************************************************************************/

static pthread_once_t g_init = PTHREAD_ONCE_INIT;
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;
static int g_haveTrackballLight = 0;
static int g_backlight = 255;
static int g_buttons = 0;
static int g_keyboard = 0;
static int g_battery = 0;

char const *const LCD_BACKLIGHT_PATH
= "/sys/class/leds/lcd-backlight";
char const *const BUTTONS_BACKLIGHT_PATH
= "/sys/class/leds/button-backlight";
char const *const KEYBOARD_BACKLIGHT_PATH
= "/sys/class/leds/keyboard-backlight";
char const *const BATTERY_BACKLIGHT_PATH
= "/sys/class/leds/battery-backlight";

char lcdBacklightPath[PROPERTY_VALUE_MAX];
char buttonsBacklightPath[PROPERTY_VALUE_MAX];
char keyboardBacklightPath[PROPERTY_VALUE_MAX];
char batteryFullBacklightPath[PROPERTY_VALUE_MAX];
char batteryInChargeBacklightPath[PROPERTY_VALUE_MAX];

/*
 * Get from the system property the backlight path
 *
 */
void init_backlight_path()
{
    property_get("backlight.lcd", lcdBacklightPath, LCD_BACKLIGHT_PATH);
    property_get("backlight.button", buttonsBacklightPath,
                 BUTTONS_BACKLIGHT_PATH);
    property_get("backlight.keyboard", keyboardBacklightPath,
                 KEYBOARD_BACKLIGHT_PATH);
    property_get("backlight.battery_full", batteryFullBacklightPath,
                 BATTERY_BACKLIGHT_PATH);
    property_get("backlight.battery_in_charge", batteryInChargeBacklightPath,
                 BATTERY_BACKLIGHT_PATH);
}

/**
 * device methods
 */

void init_globals(void)
{
    // init the mutex
    pthread_mutex_init(&g_lock, NULL);
    init_backlight_path();
}

static int
write_int(char const* path, int value)
{
    int fd;
    static int already_warned = 0;

    fd = open(path, O_RDWR);
    if (fd >= 0)
    {
        char buffer[20];
        int bytes = sprintf(buffer, "%d\n", value);
        int amt = write(fd, buffer, bytes);
        close(fd);
        return amt == -1 ? -errno : 0;
    }
    else
    {
        if (already_warned == 0)
        {
            LOGE("write_int failed to open %s\n", path);
            already_warned = 1;
        }
        return -errno;
    }
}

static int read_int(char const *path)
{
    int fd;

    fd = open(path, O_RDONLY);
    if (fd >= 0)
    {
        char buffer[20];
        int amt = read(fd, buffer, sizeof(buffer));
        close(fd);
        if (amt == -1)
            return -errno;
        buffer[amt] = 0;
        return atoi(buffer);
    }
    else
        return -errno;
}

int read_property(char *path, char *name)
{
    int max = MAX_BRIGHTNESS;
    char filename[PROPERTY_VALUE_MAX];

    strcpy(filename, path);
    strncat(filename, name, PROPERTY_VALUE_MAX);

    max = read_int(filename);

    return max;
}

int write_property(char *path, char *name, int value)
{
    int ret;
    char filename[PROPERTY_VALUE_MAX];

    strcpy(filename, path);
    strncat(filename, name, PROPERTY_VALUE_MAX);

    ret = write_int(filename, value);

    return ret;
}

static int
is_lit(struct light_state_t const* state)
{
    return state->color & 0x00ffffff;
}

static int
rgb_to_brightness(struct light_state_t const* state)
{
    int color = state->color & 0x00ffffff;
    return ((77*((color>>16)&0x00ff))
            + (150*((color>>8)&0x00ff)) + (29*(color&0x00ff))) >> 8;
}

static int
set_light_backlight(struct light_device_t* dev,
                    struct light_state_t const* state)
{
    int err = 0;
    int max;

    int brightness = rgb_to_brightness(state);
    pthread_mutex_lock(&g_lock);
    max = read_property(lcdBacklightPath, "/max_brightness");
    g_backlight = brightness;
    brightness = brightness * max / MAX_BRIGHTNESS;
    err = write_property(lcdBacklightPath, "/brightness", brightness);
    pthread_mutex_unlock(&g_lock);
    return err;
}

static int
set_light_keyboard(struct light_device_t* dev,
                   struct light_state_t const* state)
{
    int err = 0;
    int max;
    int on = is_lit(state);

    pthread_mutex_lock(&g_lock);
    on = on ? MAX_BRIGHTNESS : 0;
    g_keyboard = on;
    LOGD("Set light buttons %s %s (%d)", buttonsBacklightPath,
         on ? "ON" : "OFF", on);
    err = write_property(keyboardBacklightPath, "/brightness", on);
    pthread_mutex_unlock(&g_lock);
    return err;
}

static int
set_light_buttons(struct light_device_t* dev,
                  struct light_state_t const* state)
{
    int err = 0;
    int max;
    int on = is_lit(state);
    pthread_mutex_lock(&g_lock);
    /*
     * We share the same light so if the battery is oncharge,
     * skip setting of light battery
     */
    if (g_battery)
        goto out;

    g_buttons = on;
    on = on ? MAX_BRIGHTNESS : 0;
    LOGD("Set light buttons %s %s (%d)", buttonsBacklightPath,
         on ? "ON" : "OFF", on);
    err = write_property(buttonsBacklightPath, "/brightness", on);
out:
    pthread_mutex_unlock(&g_lock);
    return err;
}

#define BATTERY_LOW_ARGB 0xFFFF0000
#define BATTERY_MEDIUM_ARGB 0xFFFFFF00
#define BATTERY_FULL_ARGB 0xFF00FF00

static int
set_light_battery(struct light_device_t* dev,
                  struct light_state_t const* state)
{
    int err = 0;
    int max;
    int on = is_lit(state);

    g_battery = on;

    on = on ? MAX_BRIGHTNESS : 0;

    pthread_mutex_lock(&g_lock);

    err = write_property(batteryFullBacklightPath, "/brightness", 0);
    err = write_property(batteryInChargeBacklightPath, "/brightness", 0);
    if (!on)
        goto out;

    if ((unsigned int)state->color == BATTERY_FULL_ARGB) {
        err = write_property(batteryFullBacklightPath, "/brightness", on);
        LOGD("Set battery FULL %s %s (%d)", batteryFullBacklightPath,
             on ? "ON" : "OFF", on);
    } else {
        err = write_property(batteryInChargeBacklightPath, "/brightness", on);
        LOGD("Battery charging or low buttons %s %s (%d)",
             batteryInChargeBacklightPath,
             on ? "ON" : "OFF", on);
    }
out:
    pthread_mutex_unlock(&g_lock);
    return err;
}

/** Close the lights device */
static int
close_lights(struct light_device_t *dev)
{
    if (dev)
    {
        free(dev);
    }
    return 0;
}


/******************************************************************************/

/**
 * module methods
 */

/** Open a new instance of a lights device using name */
static int open_lights(const struct hw_module_t* module, char const* name,
                       struct hw_device_t** device)
{
    int (*set_light)(struct light_device_t* dev,
                     struct light_state_t const* state);

    if (0 == strcmp(LIGHT_ID_BACKLIGHT, name))
    {
        set_light = set_light_backlight;
    }
    else if (0 == strcmp(LIGHT_ID_KEYBOARD, name))
    {
        set_light = set_light_keyboard;
    }
    else if (0 == strcmp(LIGHT_ID_BUTTONS, name))
    {
        set_light = set_light_buttons;
    }
    else if (0 == strcmp(LIGHT_ID_BATTERY, name))
    {
        set_light = set_light_battery;
    }
    else
    {
        return -EINVAL;
    }

    pthread_once(&g_init, init_globals);

    struct light_device_t *dev = malloc(sizeof(struct light_device_t));
    memset(dev, 0, sizeof(*dev));

    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 0;
    dev->common.module = (struct hw_module_t*)module;
    dev->common.close = (int (*)(struct hw_device_t*))close_lights;
    dev->set_light = set_light;

    *device = (struct hw_device_t*)dev;
    return 0;
}


static struct hw_module_methods_t lights_module_methods =
{
    .open =  open_lights,
};

/*
 * The lights Module
 */
const struct hw_module_t HAL_MODULE_INFO_SYM =
{
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,
    .version_minor = 0,
    .id = LIGHTS_HARDWARE_MODULE_ID,
    .name = "GTA02 lights Module",
    .author = "Michael Trimarchi <michael@panicking.kicks-ass.org>",
    .methods = &lights_module_methods,
};
